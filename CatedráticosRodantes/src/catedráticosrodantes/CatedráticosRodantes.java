
package catedráticosrodantes;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.Scanner;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.text.Text;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javax.swing.JOptionPane;

public class CatedráticosRodantes extends Application {

    static Connection conexión = null;
    static Statement acción = null;
    static Scanner scan = new Scanner(System.in);

    @Override
    public void start(Stage primaryStage) {
        Button btn = new Button();
        btn.setText("Insertar catedráticos");
        Button btn1 = new Button();
        btn1.setText("Borrar catedrático");
        Button btn2 = new Button();
        btn2.setText("Actulizar catedrático");
        Button btn3 = new Button();
        btn3.setText("Listar catedrático");
        btn2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    acción.executeUpdate("update catedráticos set nombre ='" + dato("nombre") + "', apellido='"
                            + dato("apellido") + "' where dni ='" + dato("DNI")+"'");
                } catch (Exception e) {
                    System.out.println("te la rifaste");
                    e.printStackTrace();
                }
            }
        });

        btn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                try {
                    acción.executeUpdate("insert into catedráticos values " + nuevoCatedrático());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        btn1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    acción.executeUpdate("delete from catedráticos where dni = '" + dato("DNI")+"'");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        btn3.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ResultSet rows = null;
                try {
                    rows = acción.executeQuery("Select "+dato("columna")+" from catedráticos");
                    TableView root2=new TableView();
                    root2.setItems((ObservableList)rows.getArray("dni"));
                    Scene scene2=new Scene(root2,400,250);
                    primaryStage.setScene(scene2);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        HBox top=new HBox(btn,btn1,btn2,btn3);
        BorderPane root = new BorderPane();
        root.setTop(top);
        
        Scene scene = new Scene(root, 400, 250);

        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
        
    }

    public static void main(String[] args) {
        try {
            conexión = DriverManager.getConnection("jdbc:mysql://localhost/universidad?serverTimezone=UTC", "root", "root");
            Class.forName("com.mysql.cj.jdbc.Driver");
            acción = conexión.createStatement();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        launch(args);

    }

    static String dato(String dato) {
        return JOptionPane.showInputDialog("Ingrese el " + dato);
    }

    String nuevoCatedrático() {
        Catedráticos cate = new Catedráticos();
        return cate.insertAllColumn();
    }

}
