
package catedráticosrodantes;
import java.util.Scanner;
import javax.swing.JOptionPane;

public class Catedráticos {
    Scanner scan=new Scanner(System.in);
    private String dni;
    private String nombre;
    private String apellido;

    public String getDni() {
        return dni;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }
    
    void setDni(){
        String dni="";
        do{
            dni=dato("DNI");
        }while(dni.length()!=9);
        this.dni=dni;
    }
    
    void setNombre(){
        String nombre="";
        do{
            nombre=dato("Nombre");
        }while(nombre.length()>45);
        this.nombre=nombre;
    }
    
    void setApellido(){
        String apellido="";
        do{
            apellido=dato("apellido");            
        }while(apellido.length()>60);
        this.apellido=apellido;
    }
    
    Catedráticos(){
        setDni();
        setNombre();
        setApellido();
        
    }
    
    String dato(String dato){
        return JOptionPane.showInputDialog("Ingresa el"+dato);
    }
    
    String insertAllColumn(){
        return "('"+this.dni+"','"+this.nombre+"','"+this.apellido+"')";
    }    
}
